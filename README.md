# Pascal Hexagon data and demo stage

by Eduardo Mezêncio

## Introduction

This package contains the game font, sound effects and a demo stage with its
soundtrack. All files in this package must be in a folder named **data** in the
same folder as the executable.

Maybe some there will be a set of stages packed here. The demo stage here is not
very exciting, really.

## License

### Font

**Files**:
```
biu22-page0.tga,
biu45-page0.tga
```

"Bump IT UP" by **Aaron Amar** is licensed under CC BY-SA 3.0 / Pixmaps extracted
from the original.
https://fontstruct.com/fontstructions/show/155156/bump_it_up

### Music

**File**:
```
music/audio/blast.ogg
```

"Blast" from the album "Gameboy Love EP" by **Buskerdroid** is licensed under
CC BY-NC-ND 4.0 / Ogg encoded from original FLAC.
https://soundcloud.com/buskerdroid_chiptune
https://www.daheardit-records.net/en/discography/dhr-16#release

### Voice

**Files**:
```
begin.ogg,
excellent.ogg,
game_over_1.ogg,
game_over_2.ogg,
game_over_3.ogg,
game_over_4.ogg,
game_over_5.ogg,
hexagon.ogg,
line.ogg,
pentagon.ogg,
ph.ogg,
square.ogg,
triangle.ogg
```

by **Pedro Esteves**

This work is licensed under the Creative Commons Attribution-ShareAlike 4.0
International License. To view a copy of this license, visit
http://creativecommons.org/licenses/by-sa/4.0/ or send a letter to Creative
Commons, PO Box 1866, Mountain View, CA 94042, USA.

### Sfx

**Files**: 
```
sfx_begin.ogg,
sfx_collision.ogg,
sfx_new_record.ogg,
sfx_rotate.ogg,
sfx_start.ogg
```

by **Eduardo Mezêncio**

This work is licensed under the Creative Commons Attribution-ShareAlike 4.0
International License. To view a copy of this license, visit
http://creativecommons.org/licenses/by-sa/4.0/ or send a letter to Creative
Commons, PO Box 1866, Mountain View, CA 94042, USA.

